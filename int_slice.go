package main

// merge takes two sorted slices of ints and merges their results into another sorted slice.
// This is akin to the merge step of mergesort.
func merge(a, b []int) []int {
	retSlice := make([]int, 0, len(a)+len(b))
	ai, bi := 0, 0
	for ai < len(a) || bi < len(b) {
		// Getting rid of duplicates
		if len(retSlice) > 0 {
			if ai < len(a) && a[ai] == retSlice[len(retSlice)-1] {
				ai++
				continue
			}
			if bi < len(b) && b[bi] == retSlice[len(retSlice)-1] {
				bi++
				continue
			}
		}
		// Find next element to merge
		if bi == len(b) || (ai < len(a) && a[ai] < b[bi]) {
			retSlice = append(retSlice, a[ai])
			ai++
		} else {
			retSlice = append(retSlice, b[bi])
			bi++
		}
	}
	return retSlice
}
