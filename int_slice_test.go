package main

import "testing"

func testEqualInts(a, b []int) bool {
	if a == nil && b == nil {
		return true
	}
	if a == nil || b == nil {
		return false
	}
	if len(a) != len(b) {
		return false
	}
	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func TestMerge(t *testing.T) {
	mergeTests := []struct {
		input  [][]int
		output []int
	}{
		{input: [][]int{
			[]int{1, 2, 3, 4, 5, 6},
		}, output: []int{1, 2, 3, 4, 5, 6}},
		{input: [][]int{
			[]int{1, 2, 3, 4, 5, 6},
			[]int{1, 2, 3, 4, 5, 6},
			[]int{1, 2, 3, 4, 5, 6},
		}, output: []int{1, 2, 3, 4, 5, 6}},
		{input: [][]int{
			[]int{1, 1, 1, 1, 1, 1},
			[]int{1, 2, 2, 2, 2, 2},
			[]int{1, 2, 3, 3, 3, 3},
		}, output: []int{1, 2, 3}},
		{input: [][]int{
			[]int{1, 2},
			[]int{3, 4},
			[]int{5, 6},
		}, output: []int{1, 2, 3, 4, 5, 6}},
		{input: [][]int{
			[]int{1},
			[]int{1},
			[]int{1},
		}, output: []int{1}},
		{input: [][]int{
			[]int{1, 2},
			[]int{1},
			[]int{1, 10},
		}, output: []int{1, 2, 10}},
		{input: [][]int{
			[]int{},
			[]int{},
			[]int{},
		}, output: []int{}},
		{input: [][]int{}, output: []int{}},
	}

	for _, tt := range mergeTests {
		res := make([]int, 0)
		for _, nums := range tt.input {
			res = merge(res, nums)
		}
		if !testEqualInts(res, tt.output) {
			t.Errorf("merge(%v) failed\n expected: %v\n got: %v", tt.input, tt.output, res)
		}
	}
}
