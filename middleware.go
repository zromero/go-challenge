package main

import (
	"log"
	"net/http"
	"time"
)

// DeadlineLogger is a middleware that logs to the console if a request takes
// longer than the specified deadline.
func DeadlineLogger(duration time.Duration, h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		t := time.Now()

		h.ServeHTTP(w, r)

		d := time.Now().Sub(t)
		if d > duration {
			log.Printf("[%s] %q took longer than expected: %s/%s", r.Method, r.URL.String(), d, duration)
		}
	}
}
