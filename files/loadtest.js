// This is the load test file I used in conjuction with the k6 load test tool to make sure the 
// service performs well under load.
import { check, sleep } from "k6";
import http from "k6/http";
import { Trend } from "k6/metrics";

export let TrendRTT = new Trend("RTT");

export let options = {
	thresholds: {
    "RTT": [
      "p(99)<500",
    ],
  }
};

export default function() {
  let res = http.get("http://127.0.0.1:8080/numbers?u=http://127.0.0.1:8090/primes&u=http://127.0.0.1:8090/fibo&u=http://127.0.0.1:8090/odd");
  TrendRTT.add(res.timings.duration);
  check(res, {
    "is status 200": (r) => r.status === 200
  });
}
