package main

import (
	"context"
	"encoding/json"
	"flag"
	"log"
	"net/http"
	_ "net/http/pprof"
	"sort"
	"strings"
	"sync"
	"time"
)

// NumList represents the basic responses used by all the endpoints involved
// in this project
type NumList struct {
	Numbers []int `json:"numbers"`
}

// NumRetriever represents the method to match a parameter query to a list of numbers.
type NumRetriever interface {
	RetrieveNumbers(ctx context.Context, loc string) (*NumList, error)
}

// NumberEnv contains all of the external dependencies required for the numbers
// endpoint.
type NumberEnv struct {
	NumRetriever
}

// getNumbers is in charge of managing the concurrency for performing the requests
// to get numbers.
func (env *NumberEnv) getNumbers(ctx context.Context, urls []string) []int {
	numListChan := make(chan *NumList, len(urls))
	var wg sync.WaitGroup

	for _, u := range urls {
		wg.Add(1)
		go func(u string) {
			nums, err := env.RetrieveNumbers(ctx, u)
			if err != nil {
				// log.Printf("Unable to retrieve numbers from %s: %v\n", u, err)
				wg.Done()
				return
			}
			// If timeout dont write
			select {
			case <-ctx.Done():
				wg.Done()
				return
			default:
			}
			sort.Ints(nums.Numbers)

			numListChan <- nums
		}(u)
	}

	wgDone := make(chan struct{})
	go func() {
		wg.Wait()
		wgDone <- struct{}{}
	}()

	// goroutine that merges the returned lists of numbers.
	resMux := &sync.Mutex{}
	result := make([]int, 0)
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-wgDone:
				return
			case numList := <-numListChan:
				resMux.Lock()
				result = merge(result, numList.Numbers)
				resMux.Unlock()
				wg.Done()
			}
		}
	}()

	// Wait for either the context timeout or all of the data to be processed.
	select {
	case <-ctx.Done():
	case <-wgDone:
	}

	resMux.Lock()
	retSlice := make([]int, len(result))
	copy(retSlice, result)
	resMux.Unlock()
	return retSlice
}

// ServeHTTP is the handler which sets up the context, performs the request given
// the list of urls and writes the response back.
func (env *NumberEnv) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	startTime := time.Now()
	if r.Method != "GET" {
		w.Header().Add("Allow", "GET")
		w.WriteHeader(405)
		return
	}
	w.Header().Set("Content-Type", "application/json")

	// Setup context with timeout
	ctx, cancel := context.WithTimeout(r.Context(), 400*time.Millisecond)
	defer cancel()
	r = r.WithContext(ctx)

	// Generate the list of numbers
	resp := NumList{
		Numbers: env.getNumbers(r.Context(), r.URL.Query()["u"]),
	}

	respJSON, err := json.Marshal(resp)
	if err != nil {
		log.Println("unable to marshal response:", respJSON, time.Now().Sub(startTime))
		return
	}

	w.Write(respJSON)
}

func main() {
	port := flag.String("port", ":8080", "port to listen on")
	flag.Parse()
	if !strings.HasPrefix(*port, ":") {
		*port = ":" + *port
	}
	env := NumberEnv{&HTTPRetriever{http.DefaultClient}}
	http.HandleFunc("/numbers", DeadlineLogger(time.Millisecond*500, env.ServeHTTP))
	log.Println("running server on port ", *port)
	log.Fatal(http.ListenAndServe(*port, nil))
}
