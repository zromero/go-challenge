/*
This project contains my submimssion for github.com/travelaudience/go-challenge.

The solution is a http server with one handler that takes in a url, finds its
query strings, performs a get request to the various components, and aggregates the
results. If results aren't obtained in time, curerent obtained results are returned
instead. Here are some notes concerning the project.

Each GET requets is spawned in it's own goroutine which fetches the data and returns
it on a channel. It first sorts the array and then does the merge step of the merge
sort alogirthm to combine the results.  The GET request is given a context with an
appropriate timeout. When the timeout is reached, the program takes all processed arrays
and returns them as is.

While working on this, with the help of pprof (see docs folder) I realized that the amount
of time it takes to sort the nubers puts a hard limit on the amout of work this service can
do. A slice for 1,000,000 numbers can be sorted in around 200ms so for any request at most
a few million numbers can be processed.  Once it gets past this number, the sort algorithm
takes up too much time and starves the other goroutines.

Another design decision that went into this was to decouple the retrieval of the numbers
(via http GET) from the work splitter. An interface called NumRetriever specifies how to
map a string to a list of numbers. Doing this allowed the mockup of various endpoint behavior
easily. In retrospect, a mock server could have been just as easy but this decoupling could
make other forms of number retrieval (via websockets, rpc, etc.) just as easy.

One of the major questiosn I faced was the scale that this endpoint would be used: how many
numbers would a typical endpoint return? How many different endpoints would be called per
request? How much load is this service expected to recive?
large loads.

This project also makes use of a logger middleware that only logs if it doesn't complete in
time. This way, someone from looking at the logs could easily determine if the service is
performing up to expectations.

Some list of ambiguities and decisions I made:

What is the scale that this endpoint is expected to handle?
- I did my best to make it handle large loads. Requests can handle single requests with
  hundreds of thousands of numbers from endponts, as well as a rate of 460 requests per
  second returning tens of results.

Should partily procesed lists be acceptable?
- I decided that results from endpoints should be indivisable.

Are relative urls acceptable?
- This doesn't really make sense unless there are other endpoints on this service.

Should the service be able to call itself?
- While this would be kind of silly but I didnt' to anything to prevent it.

*/

package main
