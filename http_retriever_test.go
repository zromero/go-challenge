package main

import (
	"context"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"
)

var (
	mux    *http.ServeMux
	server *httptest.Server
)

func setup() func() {
	mux = http.NewServeMux()
	server = httptest.NewServer(mux)
	return func() {
		server.Close()
	}
}

func TestRetrieveNumbers(t *testing.T) {
	teardown := setup()
	defer teardown()

	httpRetriever := &HTTPRetriever{http.DefaultClient}
	mux.HandleFunc("/odds", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"numbers": [1, 3, 5, 7, 9]}`))
	})
	mux.HandleFunc("/evens", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`{"numbers": [2, 4, 6, 8, 10]}`))
	})
	mux.HandleFunc("/buggy", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write([]byte(`"numbers: [, 4, 6, 8, 10]}`))
	})
	mux.HandleFunc("/laggy", func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		time.Sleep(time.Millisecond * 1000)
		w.Write([]byte(`{"numbers": [2, 4, 6, 8, 10]}`))
	})

	retrieveNumbersTests := []struct {
		url             string
		expectedNumbers []int
		isError         bool
	}{
		{url: "/odds", expectedNumbers: []int{1, 3, 5, 7, 9}, isError: false},
		{url: "/evens", expectedNumbers: []int{2, 4, 6, 8, 10}, isError: false},
		{url: "/buggy", expectedNumbers: nil, isError: true},
		{url: "/laggy", expectedNumbers: nil, isError: true},
	}

	for _, tt := range retrieveNumbersTests {
		ctx, cancel := context.WithTimeout(context.Background(), time.Millisecond*500)
		numList, err := httpRetriever.RetrieveNumbers(ctx, server.URL+tt.url)
		if tt.isError && err == nil {
			t.Errorf("Expected error but no error returned.")
		} else if !tt.isError && err != nil {
			t.Errorf("Expected no error but error \"%s\" returned.", err.Error())
		} else if (numList != nil && tt.expectedNumbers != nil) && !testEqualInts(numList.Numbers, tt.expectedNumbers) {
			t.Errorf("Incorrect number list returned:\n Got: %v\n Expected %v\n", numList.Numbers, tt.expectedNumbers)
		}
		cancel()
	}
}
