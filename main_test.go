package main

import (
	"context"
	"math/rand"
	"strconv"
	"strings"
	"testing"
	"time"
)

// SimpleRetriever is a testing helper that, instead of getting a list of numbers
// by performing a GET requet to some endpoint, returns a list of numbers from a
// to b where a and b are passed in the form "a-b" in the string.
type SimpleRetriever struct{}

// RetrieveNumbers takes a string and returns a number list from a to b, separated by -, inclusively.
// As an example, "10-15" would return &NumList{Numbers: []int{10, 11, ..., 15}}.
func (r SimpleRetriever) RetrieveNumbers(ctx context.Context, loc string) (*NumList, error) {
	retList := &NumList{}
	parts := strings.Split(loc, "-")
	a, err := strconv.Atoi(parts[0])
	if err != nil {
		return nil, err
	}
	b, err := strconv.Atoi(parts[1])
	if err != nil {
		return nil, err
	}
	for i := a; i <= b; i++ {
		retList.Numbers = append(retList.Numbers, i)
	}

	return retList, nil
}

// GetNumbersTest tests if the getNumbers function and retrieve lists of numbers and combine them in order
func TestGetNumbers(t *testing.T) {
	// Instead of creating urls to return every type possible number, here the SimpleRetriever struct, which
	// implements NumRetriever, is used to make the pseudo url "1-4" return a list of nubmers 1 thorugh 4
	env := NumberEnv{SimpleRetriever{}}
	simpleNumberTests := []struct {
		urls     []string
		expected []int
	}{
		{urls: []string{"1-4", "3-8", "8-10"}, expected: []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}},
		{urls: []string{"1-1", "2-2", "7-7"}, expected: []int{1, 2, 7}},
		{urls: []string{"1-10", "1-10", "1-11"}, expected: []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11}},
	}

	for _, tt := range simpleNumberTests {
		result := env.getNumbers(context.TODO(), tt.urls)
		if !testEqualInts(tt.expected, result) {
			t.Errorf("Test %v failed.\n Expected: %v,\n Obtained: %v.\n", tt.urls, tt.expected, result)
		}
	}
}

// NRandomRetriever mocks calling an endpoint that returns n random numbers.
type NRandomRetriever struct {
	randomInts []int
	delay      int
	offset     int
}

func NewNRandomRetriever(delay int, offset int) *NRandomRetriever {
	r := &NRandomRetriever{}
	for n := 0; n < 10000000; n++ {
		r.randomInts = append(r.randomInts, rand.Intn(1e9))
	}
	r.delay = delay
	r.offset = offset
	return r
}

func (r NRandomRetriever) GetNRandom(n int) []int {
	retSlice := make([]int, 0, n)
	copy(retSlice, r.randomInts[0:n]) // retSlice = //append(retSlice, r.randomInts[0:n]...)
	return retSlice
}

// RetrieveNumbers takes the integer representation of loc and returns that many random numbers.
// Random wait is added to simulate network conditions.
func (r NRandomRetriever) RetrieveNumbers(ctx context.Context, loc string) (*NumList, error) {
	n, err := strconv.Atoi(loc)
	if err != nil {
		return nil, err
	}

	retList := &NumList{}
	retList.Numbers = r.GetNRandom(n)

	// Simpulate network delay
	select {
	case <-ctx.Done():
	case <-time.After(time.Millisecond * time.Duration(rand.Intn(r.delay)+r.offset)):
	}

	return retList, nil
}

// GetNumbersDeadlineTest tests to see if a called endpoint that takes an unknown amout of time to
// resolve and has an unbounded limit on the numbers it can return will return within the 500ms deadline.
func TestGetNumbersDeadline(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test for short mode")
	}
	env := NumberEnv{NewNRandomRetriever(550, 0)}
	for n := 0; n < 10; n++ {
		ctx := context.Background()
		ctx, cancel := context.WithTimeout(ctx, time.Millisecond*480)
		url := "100000"
		// Simulate calling four urls that return n random numbers
		t1 := time.Now()
		res := env.getNumbers(ctx, []string{url, url, url})
		diff := time.Now().Sub(t1)
		if diff > time.Millisecond*500 {
			t.Errorf("getNumbers n=%s failed to finish in 500ms. Ran in %v. Sorted %d numbers.\n", url, diff, len(res))
		}
		cancel()
	}
}

// TestGetNumbersLastMinute uses NRandomRetriever to generate a delay between 400 and 410 ms and
// generates 100,000 different numbers. This tests to make sure many numbers being generated at
// the last moment doesn't cause the function to pass its deadline.
func TestGetNumbersLastMinute(t *testing.T) {
	if testing.Short() {
		t.Skip("skipping test for short mode")
	}
	env := NumberEnv{NewNRandomRetriever(10, 400)}
	for n := 0; n < 10; n += 1000 {
		ctx := context.Background()
		ctx, cancel := context.WithTimeout(ctx, time.Millisecond*480)
		url := "100000"
		// Simulate calling four urls that return n random numbers
		t1 := time.Now()
		res := env.getNumbers(ctx, []string{url, url, url, url})
		diff := time.Now().Sub(t1)
		if diff > time.Millisecond*500 {
			t.Errorf("getNumbers n=%s failed to finish in 500ms. Ran in %v. Sorted %d numbers.\n", url, diff, len(res))
		}
		cancel()
	}

}
