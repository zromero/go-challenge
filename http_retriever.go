package main

import (
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/url"
)

// HTTPRetriever is the default method to obtain a number list from parameter query
// via HTTP GET request
type HTTPRetriever struct {
	client *http.Client
}

// isValidURL determins weather or not a url passed in to be GETted is valid or not.
// In this project only full urls are valid ('/numbers' would be rejected for example).
func isValidURL(u string) bool {
	url, err := url.Parse(u)
	if err != nil {
		return false
	}
	if url.Host == "" {
		return false
	}
	return true
}

// RetrieveNumbers retrieves a list of numbers using a URL string as a location to
// to send a GET requet to. Is cancelable via context.
func (r *HTTPRetriever) RetrieveNumbers(ctx context.Context, loc string) (*NumList, error) {
	if !isValidURL(loc) {
		return nil, errors.New("unacceptable url")
	}
	req, err := http.NewRequest("GET", loc, nil)
	if err != nil {
		return nil, err
	}
	req = req.WithContext(ctx)
	r.client.Transport = &http.Transport{}

	resp, err := r.client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return nil, errors.New("non-200 response code")
	}

	d, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	retList := &NumList{}
	err = json.Unmarshal(d, retList)
	if err != nil {
		return nil, err
	}

	return retList, nil
}
